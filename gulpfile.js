'use strict';

// Default Gulp task

// required modules
require('require-dir')('./gulp', {recurse: true});
var gulp = require('gulp'),
	sass = require('gulp-sass');



// Default task for local development
gulp.task('default', ['clean', 'html', 'style', 'serve', 'watch']);

// Default task to clean up all generated files
gulp.task('clean', ['html:clean', 'style:clean', 'styleguide:clean']);