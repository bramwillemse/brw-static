# Bramster 6

The 6th version of my personal website. This time trying to build it as a statically generated website, instead of WordPress. 

It will include:
- a timeline module (for CV / portfolio purposes)
- a static blog
- pages

1. Run npm install from the root folder
2. Run gem install jekyll (http://jekyllrb.com/docs/installation/) 
3. Run gulp to generate and serve the site

Work in progress.