'use strict';

// Required modules
var gulp        = require('gulp'),
	browserSync = require('browser-sync');



// Settings
var	reload 		= browserSync.reload;



// Task: Static Server + watching scss/html files
gulp.task('watch', function() {
    // Watch html
    gulp.watch('./src/html/**/*.html', [
	    	'html', 
	    	reload 
    	]);

    gulp.watch([
        './src/index.html', 
        './src/_includes/*.html', 
        './src/_layouts/*.html', 
        './src/*.md',
        './src/_posts/*'
    ], ['jekyll:rebuild']);

	// Watch stylesheets
    gulp.watch('./src/scss/**/*.scss', ['style']);

    // Watch scripts
    gulp.watch('./src/js/**/*', ['scripts']);

    // Watch image files
    gulp.watch('./src/images/raster/*', ['images']);

    // Watch SVG files
    gulp.watch('./src/images/vector/*', ['svgs']);

});