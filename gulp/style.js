'use strict';

// Stylesheets

// Required modules
var gulp 			= require('gulp'),
	sass 			= require('gulp-sass'),
	sourcemaps 		= require('gulp-sourcemaps'),
	prefix			= require('gulp-autoprefixer'),
	clean			= require('gulp-rimraf'),
	minify 			= require('gulp-minify-css'),
	rename 			= require('gulp-rename'),
	browserSync 	= require('browser-sync');




// Sub-task Clean compressed stylesheets
gulp.task('style:clean', function() {
	return gulp.src('./dist/css/**/*', { read: false }) // much faster 
	   	.pipe(clean());
});



// Task: Compile sass into CSS & auto-inject into browsers
gulp.task('style:compile', function() {
    return gulp.src('./src/scss/**/*.scss')
    	.pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(prefix('last 2 version'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./src/css'))
		// .pipe(browserSync.stream());
});



gulp.task('style:compress', function() {
	return gulp.src('./src/css/*.css')
		.pipe(minify())
	    .pipe(rename({
	    	suffix: '.min' 
	    }))
		.pipe(gulp.dest('./dist/css'));
})



// create combined style task
gulp.task('style', [
	'style:clean',
	'style:compile',
    'style:compress'
]);