'use strict';

// Start a server using Browsersync

// Required modules
var gulp        = require('gulp'),
	browserSync = require('browser-sync');



// Task: Static Server
gulp.task('serve', ['jekyll:build'], function() {

    browserSync.init({
        server: './dist',
        index: 'html/index.html'
    });

});