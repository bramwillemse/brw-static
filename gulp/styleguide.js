'use strict';

// Styleguide generation

// required modules
var gulp        = require('gulp'),
    styleguide  = require('sc5-styleguide'),
    sass        = require('gulp-sass'),
    clean       = require('gulp-rimraf'),
    browserSync = require('browser-sync');

// settings
var outputPath  = 'dist/styleguide',
    reload      = browserSync.reload;



gulp.task('styleguide:clean', function() {
    return gulp.src('./dist/styleguide/**/*', { read: false }) // much faster 
        .pipe(clean());

});


gulp.task('styleguide:applystyles', function() {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass({
            errLogToConsole: true
        }))
        .pipe(styleguide.applyStyles())
        .pipe(gulp.dest(outputPath));
});



gulp.task('styleguide:generate', function() {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(styleguide.generate({
            title: 'Styleguide',
            server: true,
            port: 3300,
            rootPath: outputPath,
            overviewPath: './readme.md'
        }))
        .pipe(gulp.dest(outputPath));
});

gulp.task('styleguide:watch', function(){
    gulp.watch('./src/scss/**/*.scss', ['styleguide', reload ]);
});



// this sequence is necessary
gulp.task('styleguide', [
    'styleguide:clean',
    'styleguide:generate', 
    'styleguide:applystyles',
    'styleguide:watch'
]);