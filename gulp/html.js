'use strict';

// Minification

// required modules
var gulp        = require('gulp'),
    htmlmin     = require('gulp-htmlmin'),
    shell       = require('gulp-shell'),
    clean       = require('gulp-rimraf'),
    browserSync = require('browser-sync'),
    cp          = require('child_process'),
    reload      = browserSync.reload;



// Sub-task Clean minified html
gulp.task('html:clean', function() {
	return gulp.src('./dist/html/**/*', { read: false }) // much faster 
	   	.pipe(clean({ force: true }));

});



// Sub-task jekyll build
gulp.task('jekyll:build', function (done) {
    browserSync.notify('Building Jekyll');
    return cp.spawn('jekyll', ['build'], {stdio: 'inherit'})
        .on('close', done);
});

// Sub-task jekyll rebuild and reload
gulp.task('jekyll:rebuild', ['jekyll:build'], function(){
    reload();
});



// Sub-task: Minify HTML
gulp.task('html:minify', ['jekyll:build'], function() {   
    return gulp.src('./dist/html/**/*.html')
        .pipe(htmlmin({
            collapseWhitespace: true
        }))
        .pipe(gulp.dest('./dist/html'))
        .pipe(browserSync.reload({stream:true, once: true}));

});

// Sub-task: Minify CSS
// Sub-task: Minify JS
// Sub-task: Minify Images



// create combined html task
gulp.task('html', ['html:clean', 'html:minify']);